/*########## PARCEIRO E SITEID #######################################################################################*/
SELECT * FROM riel_integracao.Parceiro WHERE ParceiroId = 24;
SELECT * FROM riel_integracao.Parceiro_Site WHERE ParceiroId = 24;
SELECT * FROM riel_integracao.Parceiro_Site WHERE SiteId = 52;

SELECT * FROM riel_producao.Site WHERE SiteId = 52;


/*########## DADOS IMPORTAÇÃO ########################################################################################*/
SELECT di.ProducaoPedidoId,di.* FROM riel_integracao.DadosImportacao di order by DataCadastro desc  WHERE di.ParceiroPedidoId = '2475535';


/*########## PAGAMENTO INTEGRAÇÃO ####################################################################################*/
SELECT * FROM riel_integracao.Pagamento pg WHERE  pg.DadosIntegracaoId = 1840912;


/*########## PEDIDO vs PRODUTO #######################################################################################*/
SELECT * FROM riel_integracao.Pedido_Produto pp WHERE pp.DadosImportacaoId = 1840912;


/*########## PRODUTO #################################################################################################*/
SELECT * FROM riel_producao.Produto p WHERE p.Codigo in ('18458');
# ("'83247','83254','85724','85729','223931','225250','225251','225252','339843','339849','339851','339853','339893','339906','418888','418895','418971','418975','419849','419851','419852','420008','420173','422318','422319','422320','422330','600863','600865','624421','624422','625439','625440','625457','1025226','1025227','1025228','1025229','1025230','1025231','1025233','1025234','1025235','1025237','1025238','1025239','1025241','1025242','1025243','1025244','1025245','1025246','1025247','1025248','1025249','1025250','1028361','1028362','1028363','1028364','1028365','1028367','1028368','1028370','1031372','702335','664398','664384','590524','405913','676846','670319','1037447','664407','1023289','684419','686919','1023320','1027703','1027702','1023900','1023285','1037426','686921','1027708','1037424','670326','1023309','670320','1023283','1027704','678797','645620','1023901','686918','647735','1023319','1027720','1023307','686914','1037448','670322','678423','684462','686920','1037429','1037451','1023295','695247','1023286','1023304','1027679','1023306','1023317','1027741','1037472','686915','1023288','664397','664378','1027681','670321','669401','695253','1023292','1027743','1027706','513345','1027680','1023313','1023303','1023302','395145','1023301','1023310','395144','405914','317683','1023300','684463','645624','1023296','1023897','405912','1023312','241104','1023314','1023311','1023896','405922','676877','678687','405915','1023298','678907','678800','405916','1027721','405920','1023294','1023308','241108','678686','676874','1023291','670323','638336','173355','173354'");

SELECT * FROM riel_producao.Produto_Site ps WHERE /*ps.ProdutoId = 575814 and*/ SiteId = 32 and status = 1;
SELECT * FROM riel_producao.Produto p WHERE p.ProdutoId = 23462;


/*########## ESTOQUE PRODUTO #########################################################################################*/
SELECT * FROM information_schema.TABLES where TABLE_NAME like '%estoq%';

SELECT * FROM riel_producao.Estoque e WHERE e.ProdutoId = 23462;
SELECT * FROM riel_producao.Estoque_Estabelecimento ee WHERE ee.ProdutoId = 23462;
update riel_producao.Estoque
SET Disponivel = 10, Total=10, Fisico = 10
where
  ProdutoId = 23462;

update riel_producao.Estoque_Estabelecimento
SET Disponivel = 10,Fisico= 10
where
  ProdutoId = 23462;



SELECT * FROM riel_producao.Estoque_Estabelecimento ee WHERE pp.DadosImportacaoId = 1840912;


select * from riel_producao.Produto_Site where SiteId = 32;
select * from riel_producao.Categoria_Site where SiteId = 32;
select * from riel_producao.Categoria_Site where SiteId = 318;


/* Produto/QryGetProdutosFullPaginado */
SELECT
            P.ProdutoId,
            P.Codigo,
            P.CodigoRetail,
            PCU.EAN,
            CONCAT(PCU.CodFornecedor, ' ') AS CodigoFornecedor,
            P.Nome,
            P.Descricao,
            P.VendeAvulso,
            P.TipoTransporte,

                        IF (C2.Nome = 'Home', C1.CategoriaId, C2.CategoriaId) AS LojaId,
            IF (C2.Nome = 'Home', C1.Nome, C2.Nome) AS Loja,

            IF (C2.Nome = 'Home', C.CategoriaId, C1.CategoriaId) AS CategoriaId,
            IF (C2.Nome = 'Home', C.Nome, C1.Nome) AS Categoria,

            IF (C2.Nome = 'Home', '', C.CategoriaId) AS SubCategoriaId,
            IF (C2.Nome = 'Home', '', C.Nome) AS SubCategoria,

            F.FabricanteId,
            F.Nome AS Fabricante,
            PS.Status,
            P.EmLinha,
            PS.PrecoDe,
            PS.PrecoPor,
            1 AS Quantidade,
            PS.ParcelamentoMaximo AS ParcelamentoMaximoSemJuros,

            (SELECT
                GROUP_CONCAT(CAST(CONCAT(CR.Nome, ': ', CV.Valor, ' ', CR.Unidade) AS CHAR)  SEPARATOR '|')
            FROM
                riel_producao.Produto_CaracteristicaValor PCV
                INNER JOIN riel_producao.CaracteristicaValor CV ON CV.CaracteristicaValorId = PCV.CaracteristicaValorId
                INNER JOIN riel_producao.Caracteristica CR ON CR.CaracteristicaId = CV.CaracteristicaId
            WHERE
                P.ProdutoId = PCV.ProdutoId
                AND CR.Nome <> '' AND CV.Valor <> '') AS Caracteristicas,

            S.Url AS UrlSite,
            PS.DataAlteracao,

            (SELECT
                GROUP_CONCAT(CAST( CONCAT(PM.Src, '::', PM.Mime) AS CHAR) ORDER BY PM.Ordem SEPARATOR '|')
             FROM
                riel_producao.ProdutoMedia PM
             WHERE PM.ProdutoId = P.ProdutoId
                AND PM.Src <> '' AND PM.Mime <> '') AS Medias,

            (SELECT GROUP_CONCAT(II.Nome ORDER BY II.ItemInclusoId SEPARATOR '|')
             FROM riel_producao.ItemIncluso II
             WHERE P.ProdutoId = II.ProdutoId) AS ItensIncluso,

             PD.Altura,
             PD.Comprimento,
             PD.Largura,
             PD.Peso,
             PD.AlturaEmbalagem,
             PD.ComprimentoEmbalagem,
             PD.LarguraEmbalagem,
             PD.PesoEmbalagem,
             IFNULL(E.Disponivel,0) AS Disponivel

        FROM
            riel_producao.Produto P
            INNER JOIN riel_producao.Fabricante F ON (F.FabricanteId = P.FabricanteId)
            INNER JOIN riel_producao.Produto_Site PS ON (P.ProdutoId = PS.ProdutoId AND PS.SiteId = 1)
            INNER JOIN riel_producao.Site S ON (PS.SiteId = S.SiteId)
            INNER JOIN riel_producao.Produto_Categoria PC ON (P.ProdutoId = PC.ProdutoId AND PC.Principal=1)
            INNER JOIN riel_producao.Categoria_Site CS ON (PC.CategoriaId = CS.CategoriaId AND CS.SiteId = 1)
            INNER JOIN riel_producao.Categoria C  ON PC.CategoriaId = C.CategoriaId
            INNER JOIN riel_producao.Categoria C1 ON C.CategoriaAsc = C1.CategoriaId
            LEFT JOIN riel_producao.Categoria C2 ON C1.CategoriaAsc = C2.CategoriaId
            LEFT JOIN riel_producao.ProdutoCusto PCU ON P.Codigo = PCU.Codigo
            LEFT JOIN riel_producao.Estoque E ON (E.ProdutoId = P.ProdutoId)
            LEFT JOIN riel_producao.ProdutoDimensao PD ON PD.ProdutoId = P.ProdutoId
            WHERE 1=1  AND PS.Status IN (1, 2)
        LIMIT 100
        OFFSET 0



SELECT
    P.*,
    PA.Aviso,
    PD.*,
    PS.*,
    P.ProdutoId as Id,
    IF(P.Situacao = 'A', 1, 0) as SituacaoProduto,
    riel_producao.EstoqueDisponivel(P.Tipo,P.ProdutoId,PS.SiteId) as Disponivel,
    (SELECT COUNT(1) FROM riel_producao.ProdutoMedia PM WHERE PM.Mime IN ('gif','jpg','jpeg','png') AND PM.ProdutoId = P.ProdutoId) AS QtdImagemCadastrada

FROM
    riel_producao.Produto P
    LEFT JOIN riel_producao.Produto_Aviso PA ON PA.ProdutoId = P.ProdutoId
    LEFT JOIN riel_producao.ProdutoDimensao PD ON PD.ProdutoId = P.ProdutoId
    INNER JOIN riel_producao.Produto_Site PS ON PS.ProdutoId = P.ProdutoId
WHERE
    1=1
  and P.Codigo = '18458'
and SiteId = 32;

SELECT Disponivel
FROM riel_producao.Estoque_Estabelecimento EE
WHERE EE.ProdutoId = '23462' AND EE.EstabelecimentoId = 2;


select * from riel_producao.Produto_Site where ProdutoId = 23462 and SiteId = 32;


SELECT
    CONCAT(S.SiteId,'_',SG.GrupoId) as Sites,
    S.SiteId as SiteId,
    SG.GrupoId as GrupoId,
    S.Nome as NomeSite,
    SG.Nome as NomeGrupo,
    S.Url as Url
FROM
    riel_producao.Site S
    INNER JOIN riel_producao.Site_Grupo SG ON S.Grupo = SG.GrupoId
     LEFT JOIN riel_producao.Admin_Usuario_Site Aus ON S.SiteId = Aus.SiteId
WHERE
    1=1
     AND Aus.UsuarioId = '1328'  AND S.Status IN (1)
ORDER BY
    S.Grupo, S.SiteId




SELECT * FROM
  riel_integracao.Parceiro_Site ps
  INNER JOIN riel_producao.Site s on
    ps.SiteId = s.SiteId
WHERE ParceiroId = 24;
