SELECT * FROM information_schema.TABLES where TABLE_NAME like '%entre%';
/*########## PARCEIRO E SITEID #######################################################################################*/
SELECT * FROM riel_integracao.Parceiro;
SELECT * FROM riel_integracao.Parceiro_Site;

/*########## MONITORAMENTO PEDIDOS POR HORA ##########################################################################*/
SELECT
  date_format(di.DataCadastro, '%Y-%m-%d') 'Data_Pedido',
  di.ParceiroSiteId 'Parceiro',
  DATE_FORMAT(di.DataCadastro,'%H') 'Hora_Pedido',
  count(*) 'Qtd Pedido/Hora'
FROM
  riel_integracao.DadosImportacao di
WHERE
  di.DataCadastro >= '2017-12-18 00:00:00' AND di.DataCadastro <= '2017-12-18 23:59:59' AND
--  date_format(di.DataCadastro, '%Y-%m-%d') = date_format(now(),'%Y-%m-%d') AND
    di.ParceiroSiteId = 33
--  AND DATE_FORMAT(di.DataCadastro,'%H')  = 16
--  and di.ParceiroPedidoId = 553852;
GROUP BY Data_Pedido, Hora_Pedido WITH ROLLUP ;

SELECT
  di.ParceiroPedidoId, di.ProducaoPedidoId,di.*
FROM
  riel_integracao.DadosImportacao di
WHERE
--  di.DataCadastro >= '2017-12-07 00:00:00' AND di.DataCadastro <= '2017-12-08 23:59:59' AND
  date_format(di.DataCadastro, '%Y-%m-%d') = date_format(now(),'%Y-%m-%d')  AND
  di.ParceiroSiteId = 33
--  AND DATE_FORMAT(di.DataCadastro,'%H')  = 16
--  and di.ParceiroPedidoId = 553852;
;

/*########## CONFIRMAÇÃO DE PAGAMENTO ################################################################################*/
SELECT
  date_format(di.DataCadastro, '%Y-%m-%d') 'Data_Pedido',
  IF(p.Resultado = 'AP','AP - APROVADO', IF(p.Resultado = 'RP','RP - REPROVADO','SEM CONFIRMAÇÃO')) 'Descricao',
  count(*) 'Quantidade'
FROM
  riel_integracao.DadosImportacao di
  INNER JOIN mv_gateway.Pedido p ON
    di.ProducaoPedidoId = p.PedidoId
WHERE
--  p.Resultado is null AND
--  p.Resultado = 'AP' AND
  di.DataCadastro >= '2017-12-07 00:00:00' AND di.DataCadastro <= '2017-12-08 23:59:59' AND
--  date_format(di.DataCadastro, '%Y-%m-%d') = date_format(now(),'%Y-%m-%d') AND
  di.ParceiroSiteId = 60
--  AND DATE_FORMAT(di.DataCadastro,'%H')  = 16
GROUP BY
  Data_Pedido , Descricao /*WITH ROLLUP*/
;


SELECT
  DISTINCT pp.ProdutoCodigo
FROM
  riel_integracao.DadosImportacao di
  INNER JOIN riel_producao.Pedido_Produto pp ON
    di.ProducaoPedidoId = pp.PedidoId
WHERE
  date_format(di.DataCadastro, '%Y-%m-%d') = date_format(now(),'%Y-%m-%d')  AND
  di.ParceiroSiteId = 57
  AND DATE_FORMAT(di.DataCadastro,'%H')  = 16
;





/*########## PEDIDOS AINDA NÃO FATURADOS  ############################################################################*/
SELECT
  DISTINCT di.ParceiroPedidoId "Pedido Parceiro", di.ProducaoPedidoId "Pedido ARC", di.DataCadastro "Data Integração", pps.PedidoStatusId "ID Status", ps.Nome "Status"
FROM
  riel_integracao.DadosImportacao di
  INNER JOIN riel_producao.Pedido p ON
    di.ProducaoPedidoId = p.PedidoId
  INNER JOIN riel_producao.Pedido_PedidoStatus pps ON
    p.PedidoId = pps.PedidoId
  INNER JOIN riel_producao.PedidoStatus ps ON
    pps.PedidoStatusId = ps.PedidoStatusId
WHERE
  di.DataCadastro >= '2017-11-01 00:00:00' AND di.DataCadastro <= '2017-12-12 23:59:59' AND
  di.ParceiroSiteId in (11,13,14,15,16,17,18,19,23) AND
  pps.PedidoStatusId = 3 AND
  pps.PedidoId NOT IN (
              SELECT
                DISTINCT pps.PedidoId
              FROM
                riel_integracao.DadosImportacao di
                INNER JOIN riel_producao.Pedido p ON
                  di.ProducaoPedidoId = p.PedidoId
                INNER JOIN riel_producao.Pedido_PedidoStatus pps ON
                  p.PedidoId = pps.PedidoId
              WHERE
                di.DataCadastro >= '2017-11-01 00:00:00' AND di.DataCadastro <= '2017-12-12 23:59:59' AND
                di.ParceiroSiteId in (11,13,14,15,16,17,18,19,23) AND
                (pps.PedidoStatusId = 4 or pps.PedidoStatusId = 7 ) )
;


SELECT
  DISTINCT di.ParceiroPedidoId "Pedido Parceiro", di.ProducaoPedidoId "Pedido ARC", di.DataCadastro "Data Integração", pps.PedidoStatusId "ID Status", ps.Nome "Status"
FROM
  riel_integracao.DadosImportacao di
  INNER JOIN riel_producao.Pedido p ON
    di.ProducaoPedidoId = p.PedidoId
  INNER JOIN riel_producao.Pedido_PedidoStatus pps ON
    p.PedidoId = pps.PedidoId
  INNER JOIN riel_producao.PedidoStatus ps ON
    pps.PedidoStatusId = ps.PedidoStatusId
WHERE
  ps.PedidoStatusId = 7 and
pps.PedidoId in ()

;



select * from riel_producao.Pedido_PedidoStatus where PedidoId in (34313892,34216844,34336886);
select * from riel_producao.PedidoStatus where PedidoId in (34313892,34216844,34336886);





