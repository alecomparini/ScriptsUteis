/*########## STATUS DA ENTREGA #######################################################################################*/
SELECT * FROM information_schema.TABLES where TABLE_NAME like '%%';

SELECT * from arc_producao.pedidos_venda pv where pv.num_controle_pedido = '33627533';
SELECT * from arc_producao.ped_venda_entregas pv where pv.id_pedido_venda = 14912511;

/*########## TERCEIRO - CADASTROS DOS CLIENTE ########################################################################*/
SELECT * FROM arc_producao.terceiros t WHERE documento


/*########## RELATÓRIO STATUS DE ENTREGA [DATA PREVSITA] #############################################################*/
SELECT
  pv.codigo_parceiro 'Pedido do Parceiro',pv.num_controle_pedido 'Pedido ARC',
  pve.datent_prometida 'Data Prometida',pvt.data_ocorrencia 'Data da Entrega',
  pve.num_controle_entrega 'Nro Entrega',

  pve.id_ponto_controle 'Status Entrega',pc.nome_ponto_controle 'Resumo da Entrega',
  pvt.id_ponto_controle 'Status Pedido',pc_pvt.nome_ponto_controle 'Rastreamento do Pedido'
#   ,pv.*, pve.*, pvt.*,pc.*
FROM
  arc_producao.pedidos_venda pv
  INNER JOIN arc_producao.ped_venda_entregas pve ON
    pv.id_pedido_venda = pve.id_pedido_venda
  INNER JOIN arc_producao.ped_venda_tracking pvt ON
    pve.id_ped_venda_entrega = pvt.id_ped_venda_entrega AND
    pve.id_ultimo_tracking = pvt.id_pedvnd_tracking
  INNER JOIN arc_producao.pontos_controle pc on
    pve.id_ponto_controle = pc.id_ponto_controle
  INNER JOIN arc_producao.pontos_controle pc_pvt on
    pvt.id_ponto_controle = pc_pvt.id_ponto_controle
WHERE
#   pv.num_controle_pedido in
#   ('33837520')
#   pve.num_controle_entrega IN
#   ('3140365802')
   pv.codigo_parceiro in
   ('B2W-265551642401')

;

select * FROM arc_producao.pedidos_venda pv where  pv.codigo_parceiro in ('B2W-106664085801');
select pve.* from arc_producao.ped_venda_entregas pve where pve.id_pedido_venda = 15410452;
select * FROM arc_producao.pontos_controle pc where id_ponto_controle = 'CAN';
select * FROM arc_producao.ped_venda_tracking pvt where pvt.id_pedvnd_tracking = 306360870;

/*########## DATA DA INCLUSÃO DO ESTOQUE NO ARC ######################################################################*/
select id_log,data_envio, le.* from arc_producao.log_estoques le
where
  id_item in
  (1027949)
--  (1039027,1028112,1027949,1027935,1036295,1028120,1028102,1028101,1033137,1027970,1028103,1036319,1036294,1036318,1036292,1036296,1036308,1036309,1036293,1036297,1036290,1036291,1038318)
  and data_envio >= '2017-12-06'
  and qtd_saldo > 0;


select id_log,data_envio, le.* from arc_producao.log_estoques le
where
  id_item in
  (1033137)
--  (1039027,1028112,1027949,1027935,1036295,1028120,1028102,1028101,1033137,1027970,1028103,1036319,1036294,1036318,1036292,1036296,1036308,1036309,1036293,1036297,1036290,1036291,1038318)
  and data_envio >= '2017-11-29'
  and qtd_saldo > 0
  and data_envio = (select max(data_envio) from arc_producao.log_estoques
                    where
                      id_item in (681653)
                      and data_envio >= '2017-11-28'
                      and qtd_saldo > 0
                    );



/*########## LOG GATEWAY NO ARC ######################################################################################*/
SELECT * FROM information_schema.COLUMNS where COLUMN_NAME like '%id_gateway_transacao%';
select pv.num_controle_pedido,
       pv.id_gateway_transacao,
       gl.dados_solicitacao
from
  arc_producao.pedidos_venda pv
  LEFT JOIN arc_producao.gateway_log gl on
    gl.id_gateway_transacao = pv.id_gateway_transacao
where pv.num_controle_pedido='33541799';


SELECT * FROM arc_producao.gateway_log where url_solicitacao = '/erp/service/ServicosVendasGatewayBO/enviarConfirmacaoPagamento';



/*########## CÁLCULO DO ESTOQUE NO ARC ###############################################################################*/
SELECT * FROM information_schema.COLUMNS where COLUMN_NAME like '%id_gateway_transacao%';



