select * from pessoa where nome like '%a%' order by nome, tipopessoa;
{"query": { "match": { "nome": "a" }}, "sort": [{ "nome": "asc", "tipopessoa": "asc" }] }
-- match --> match_phrase --> é igual o like do SQL

select * from pessoa order by nome, tipopessoa;
{"query": { "match_all": {} }, "sort": [{ "nome": "asc", "tipopessoa": "asc" }] }

-- Para ordernar campos Text no Elastic:
{"properties": {"nome": {"type": "text","fielddata": true}, "tipopessoa": {"type": "text","fielddata": true}}}



/*Meu Exemplo do Kibana - o Sort é um campo indexado dai não precisa o passo acima */
{"query":{"match":{"tagvalue3.keyword":"B2W-106659499001"}},"sort":[{"@timestamp":"desc"}]}
{"query":{"constant_score":{"filter":{"terms":{"tagvalue3.keyword":["B2W-106659185301","B2W-106659499001"]}}}}}
/*Abaixo faz o max*/
{"size":"20","query":{"constant_score":{"filter":{"terms":{"tagvalue3.keyword":["B2W-106659185301","B2W-106659499001"]}}}},"aggs":{"max_timestamp":{"max":{"field":"@timestamp"}}}}
/*unica forma q deu certo para trazer um de cada*/
{"query":{"terms":{"tagvalue3.keyword":["B2W-106659185301","B2W-106659499001","B2W-106660288503"]}},"sort":[{"@timestamp":{"order":"desc"}}],"size":3}
