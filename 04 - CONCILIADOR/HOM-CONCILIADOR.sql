SELECT * FROM information_schema.columns where table_schema like 'public' and column_name like '%id_unico_transacao%';

SELECT
   -- v.id_adquirente, sum(v.vlr_total)
  v.dta_venda, v.id_adquirente, sum(v.vlr_total)
  -- v.id, v.id_estabelecimento, v.dta_venda, dta_captura, v.qtd_parcela, id_bandeira, v.nsu_doc,
  -- v.vlr_total, v.vlr_compra_parcela, v.id_adquirente, dta_conciliacao_manual, v.dta_inclusao
FROM venda v
where
  dta_venda BETWEEN  '2018-01-02' and '2018-01-02'
  and id_estabelecimento = 43
group by
  v.dta_venda, v.id_adquirente;

SELECT * FROM arquivo_conciliacao WHERE id = 610;
SELECT * FROM arquivo_conciliacao_resumo where dta_pagamento BETWEEN '2018-01-02' and '2018-01-02' and tipo_operacao = 'PAGAMENTO';
SELECT * FROM recebivel;


SELECT v.*
  -- v.id, v.id_estabelecimento, v.dta_venda, dta_captura, v.qtd_parcela, id_bandeira, v.nsu_doc,
  -- v.vlr_total, v.vlr_compra_parcela, v.id_adquirente, dta_conciliacao_manual, v.dta_inclusao
FROM venda v
where
  dta_venda BETWEEN  '2018-01-02' and '2018-01-02'
  and id_estabelecimento = 43
  and id_adquirente = 6;

SELECT
  *
FROM
  recebivel
WHERE
  dta_pagamento BETWEEN  '2018-01-02' and '2018-01-02'
;

select
  *
from
  arquivo_conciliacao
WHERE
  opcao_extrato = 'PAGAMENTO'
  and id_adquirente = 64;


select * from arquivo_conciliacao WHERE ID = 610;
select * from arquivo_conciliacao_detalhe where id_arquivo_conciliacao = 610;
select * from arquivo_conciliacao_resumo where tipo_operacao = 'PAGAMENTO' AND dta_pagamento BETWEEN '2018-01-02' and '2018-01-02';

select * from
  arquivo_conciliacao ac
  INNER JOIN arquivo_conciliacao_resumo acr ON
    ac.id = acr.id_arquivo_conciliacao
where
  dta_pagamento BETWEEN '2018-01-02' and '2018-01-02'
  AND tipo_operacao
;

SELECT
    *
FROM
  arquivo_conciliacao ac
  inner JOIN arquivo_conciliacao_resumo acr on
    ac.id = acr.id_arquivo_conciliacao
   INNER JOIN arquivo_conciliacao_detalhe acd on
     ac.id = acd.id_arquivo_conciliacao AND
     acr.id_resumo_operacao = acd.id_unico_transacao
where
  ac.id_adquirente = 64 and
  acr.dta_pagamento BETWEEN '2018-01-02' and '2018-01-02'
  and ac.opcao_extrato = 'PAGAMENTO'
  and ac.id_estabelecimento = 43;

  id_arquivo_conciliacao = 610;


select * from recebivel where id_detalhe_pagamento=106805;

select count(*),id_detalhe_pagamento from recebivel
group by id_detalhe_pagamento
having count(*) > 1;


SELECT
  coalesce((select sum(a.vlr_compra_parcela)
from
  arquivo_conciliacao_detalhe a
    join recebivel b on
      (a.id = b.id_detalhe_pagamento)
    join arquivo_conciliacao c on
      (a.id_arquivo_conciliacao = c.id)
    join arquivo_conciliacao_resumo acr on
      (acr.id_arquivo_conciliacao=a.id_arquivo_conciliacao and acr.id_resumo_operacao = a.id_unico_transacao)
 where
  b.dta_pagamento between '2018-01-02' and '2018-01-02'
  and c.id_estabelecimento = 43
  and (c.opcao_extrato = 'PAGAMENTO' OR c.opcao_extrato = 'TODOS')
  and (acr.tipo_operacao = 'VENDA' or acr.tipo_operacao = 'PAGAMENTO' or acr.tipo_operacao = 'PG')
  and a.id_erro_conciliacao is null),0) as valor_pagamento_conciliado

SELECT
  *
from
  arquivo_conciliacao_detalhe a
    join recebivel b on
      (a.id = b.id_detalhe_pagamento)
    join arquivo_conciliacao c on
      (a.id_arquivo_conciliacao = c.id)
    join arquivo_conciliacao_resumo acr on
      (acr.id_arquivo_conciliacao=a.id_arquivo_conciliacao and acr.id_resumo_operacao = a.id_unico_transacao)
 where
  b.dta_pagamento between '2018-01-02' and '2018-01-02'
  and c.id_estabelecimento = 43
  and (c.opcao_extrato = 'PAGAMENTO' OR c.opcao_extrato = 'TODOS')
  and (acr.tipo_operacao = 'VENDA' or acr.tipo_operacao = 'PAGAMENTO' or acr.tipo_operacao = 'PG')
  and a.id_erro_conciliacao is null
  and a.id = 136377;


SELECT * FROM recebivel;

SELECT * FROM adquirente;
SELECT * FROM taxa;
SELECT * FROM estabelecimento;
SELECT * FROM produto;
SELECT * FROM arquivo_conciliacao;


SELECT * FROM arquivo_conciliacao_detalhe_erro;
SELECT * FROM consulta_nomeada;
SELECT * FROM parametro_sistema;
SELECT * FROM usuario_estabelecimento;
SELECT * FROM adquirente_bandeira;
SELECT * FROM arquivo_conciliacao_resumo;

SELECT * FROM adquirente_produto_bandeira;
SELECT * FROM menu;
SELECT * FROM produto_bandeira;
SELECT * FROM taxa_intervalo_parcela;
SELECT * FROM taxa_intervalo_valor;
SELECT * FROM estabelecimento_template_adquirente;
SELECT * FROM tipo_taxa;
SELECT * FROM tipo_template_adquirente;
SELECT * FROM template_adquirente;
SELECT * FROM bandeira;
SELECT * FROM arquivo_conciliacao_detalhe;
SELECT * FROM bandeira_alias;
SELECT * FROM tipo_transacao;
SELECT * FROM origem_ajuste;
SELECT * FROM bandeira_exclusao;
SELECT * FROM log;
SELECT * FROM menu_privilegio;
SELECT * FROM schema_version;

/*############ TABELAS DE DOMÍNIOS ###################################################################################*/
SELECT * FROM arquivo_conciliacao_status;